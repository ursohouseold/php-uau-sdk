# Bibliotéca PHP para integração com ERP UAU

## Instalação

 Adicione o repositório no composer.json.
```json
"repositories": [
  {
    "type": "git",
    "url": "https://github.com/mocallu/php-uau.git"
  }
],
```
Instale a dependência.
```bash
$ composer require mocallu/php-uau
```

## Exemplo de uso

```php
use PHPUau\Cliente;
use PHPUau\Services\Autenticador;

// Configurando o cliente
$client = new Client();
$client->setHeader('X-INTEGRATION-Authorization', 'TOKEN_DE_INTEGRAÇÃO');
$client->setBaseUri('HTTPS://URL_DA_API.COM/uau/API/v1');

// Autentica e adiciona o token no header
$autenticador = new Autenticador($client);
$response = $autenticador->login('USUARIO', 'SENHA');
$client->setHeader('Authorization', $response['data']['token']);

// Retorna os dados da pessoa logada
$dadosPessoa = $autenticador->detalhesPessoa()['data'];
var_dump($dadosPessoa);

```

## Requisições Disponíveis

1. Autenticador;
1.1 Login;
1.2 Detalhes da Pessoa;
2. Boleto;
2.1 Consulta boletos por código da Pessoa;
3.Extrato do Cliente;
3.1 Retorna arquito PDF em base64 com o extrato do cliente;
4. Pessoa;
4.1 Alterar Senha;
5.Venda;
5.1 Lista de Empreendimentos por Pessoa;