<?php
/**
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau;

use Laminas\Http\Client as HttpClient;

class Client
{
    private $client;
    private $baseUri;
    private $headers;

    public function __construct()
    {
        $this->client = new HttpClient();
        $this->headers = $this->client->getRequest()->getHeaders();
        $this->client->setOptions([
            'maxredirects' => 3,
            'timeout' => 30
        ]);
    }

    /**
     * Adiciona uma linha no header do cliente
     * @param string $name
     * @param string $value
     * @return void
     */
    public function setHeader(string $name, $value = '')
    {
        $this->headers->addHeaderLine($name, $value);
    }

    /**
     * Seta a uri base para a comunicação com a API
     * @param string $baseUri
     * @return void
     */
    public function setBaseUri(string $baseUri)
    {
        $this->baseUri = $baseUri;
        return $this->baseUri;
    }

    /**
     * Método para requisições HTTP POST
     * @param string $uri
     * @param array $params
     * @return void
     */
    public function post(string $uri, array $data)
    {
        $this->setHeader('Content-Type', 'application/json');
        $this->client->setUri($this->baseUri . $uri);
        $this->client->setRawBody(json_encode($data));
        $this->client->setMethod('POST');
        return $this->client->send();
    }

    /**
     * Método para requisições HTTP GET
     * @param string $uri
     * @param array $params
     * @return void
     */
    public function get(string $uri, $params = [])
    {
        $this->client->setUri($this->baseUri . $uri);
        $this->client->setParameterGet($params);
        $this->client->setMethod('GET');
        return $this->client->send();
    }
}
