<?php
/**
 * Classe Autenticação
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau\Services;

class Autenticador
{
    /**
     * @var \PHPUau\Client Http client
     */
    private $client;

    public function __construct(\PHPUau\Client $client)
    {
        $this->client = $client;
    }

    /**
     * Login Function
     * @param string $login
     * @param string $senha
     * @return array Resposta da requisição
     */
    public function login(string $login, string $senha)
    {
        $request = $this->client->post('Autenticador/AutenticarUsuario', [
            'login' => $login,
            'senha' => $senha,
        ]);
        return [
            'request' => $request,
            'data' => [
                'token' => str_replace('"', '', $request->getBody()),
            ],
        ];
    }

    /**
     * Detalhes Function
     * @return array Resposta da requisição
     */
    public function detalhesPessoa()
    {
        $request = $this->client->post('Autenticador/ConsultarDadosUsrLogado', []);
        $content = json_decode($request->getBody());
        return [
            'request' => $request,
            'data' => (object) [
                'dadosPessoais' => (object) [
                    'codigo' => $content[0]->dadospessoais[1]->codigo,
                    'nome'   => $content[0]->dadospessoais[1]->nome,
                    'cpf'    => $content[0]->dadospessoais[1]->cpf,
                    'email'  => $content[0]->dadospessoais[1]->email,
                    'login'  => $content[0]->dadospessoais[1]->login,
                ],
            ],
        ];
    }
}
