<?php
/**
 * Classe ExtratoDoCliente
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau\Services;

class ExtratoDoCliente
{
    /**
     * @var \PHPUau\Client Http client
     */
    private $client;

    public function __construct(\PHPUau\Client $client)
    {
        $this->client = $client;
    }

    /**
     * Retorna o extrato da pessoa em PDF Base64
     * @param integer $codigoPessoa
     * @param int $obra
     * @param int $empresa
     * @param int $numeroVenda
     * @return array Resultado da requisição
     */
    public function geraPdfExtrato(int $codigoPessoa, $obra, $empresa, $numeroVenda)
    {
        $dateNow = new \DateTime();
        $request = $this->client->post('Venda/ConsultarEmpreendimentosCliente', [
            'codigo_usuario'        => $codigoPessoa,
            'empresa'               => $empresa,
            'obra'                  => $obra,
            'numVenda'              => $numeroVenda,
            'tipoOrdenacao'         => 1,
            'valorAntecipado'       => true,
            'dataProrrogacao'       => true,
            'ocultarPersonalizacao' => true,
            'ocultarUsuario'        => true,
            'dataCalculo'           => $dateNow->format(\DateTime::ISO8601)
        ]);
        return [
            'request' => $request,
            'data'    => str_replace('"', '', $request->getBody()),
        ];
    }
}
