<?php
/**
 * Classe Venda
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau\Services;

class Venda
{
    /**
     * @var \PHPUau\Client Http client
     */
    private $client;

    public function __construct(\PHPUau\Client $client)
    {
        $this->client = $client;
    }

    /**
     * Lista os empreendimentos por pessoa
     * @param integer $codigoPessoa
     * @return array Resposta da request
     */
    public function listaEmpreendimentosPorPessoa(int $codigoPessoa)
    {
        $request = $this->client->post('Venda/ConsultarEmpreendimentosCliente', [
            'codigo_usuario' => $codigoPessoa,
        ]);
        $content = json_decode($request->getBody());
        $empreendimentos = $content[0]->MyTable;
        array_shift($empreendimentos); // Remove referência da Entidade
        return [
            'request' => $request,
            'data'    => $empreendimentos,
        ];
    }
}
