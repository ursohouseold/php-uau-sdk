<?php
/**
 * Classe ExtratoImpostoDeRenda
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau\Services;

class ExtratoImpostoDeRenda
{
    /**
     * @var \PHPUau\Client Http client
     */
    private $client;

    public function __construct(\PHPUau\Client $client)
    {
        $this->client = $client;
    }

    /**
     * gerarPdfImpostoDeRenda Function
     * @param integer $ano
     * @param integer $venda
     * @param string $obra
     * @param integer $empresa
     * @return array Resposta da request
     */
    public function gerarPdfImpostoDeRenda($ano, $venda, $obra, $empresa)
    {
        $data = [
            'vendasobras_empresa' => [
                array($venda, $obra, $empresa)
            ],
            'ano_base'              => $ano,
            'naomostradados_venda'  => true,
        ];
        $request = $this->client->post('RelatorioIRPF/GerarPDFRelIRPF', $data);
        return [
            'request' => $request,
            'data'    => str_replace('"', '', $request->getBody()),
        ];
    }
}
