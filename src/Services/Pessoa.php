<?php
/**
 * Classe Pessoa
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau\Services;

class Pessoa
{
    /**
     * @var \PHPUau\Client Http client
     */
    private $client;

    public function __construct(\PHPUau\Client $client)
    {
        $this->client = $client;
    }

    /**
     * Altera a senha da pessoa
     * @param integer $codigoPessoa
     * @param string $senha
     * @return array Resposta da request
     */
    public function alteraSenha(int $codigoPessoa, string $senha)
    {
        $request = $this->client->post('Pessoas/AlterarSenhaCliente', [
            'codigo_cliente' => $codigoPessoa,
            'senha'          => $senha,
        ]);
        return [
            'request' => $request,
            'data'    => str_replace('"', '', $request->getBody())
        ];
    }
}
