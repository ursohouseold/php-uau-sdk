<?php
/**
 * Classe Boleto
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUau\Services;

class Boleto
{
    /**
     * @var \PHPUau\Client Http client
     */
    private $client;

    public function __construct(\PHPUau\Client $client)
    {
        $this->client = $client;
    }

    /**
     * ConsultaBoletosByCodigoPessoa function
     * @param integer $codigoPessoa
     * @param boolean $vencidos
     * @return array Resultado da requisição
     */
    public function consultaBoletosByCodigoPessoa(int $codigoPessoa, $vencidos = 'true')
    {
        $request = $this->client->post('BoletoServices/ConsultarBoletosDoCliente', [
            'codPessoa'              => $codigoPessoa,
            'naoMostraBoletoVencido' => $vencidos,
            'tipo_usuario'           => 1
        ]);
        return [
            'request' => $request,
            'data' => json_decode($request->getBody())
        ];
    }
}
