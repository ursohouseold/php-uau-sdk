<?php
/**
 * @author Luã Mota <hi@mocallu.com>
 */

namespace PHPUauTest;

use Laminas\Cache\StorageFactory;

class Cache {

    private $storage;

    public function __construct()
    {
        $this->storage = StorageFactory::factory([
            'adapter' => [
                'name'      => 'filesystem',
                'options'   => [
                    'ttl'       => 60,
                    'cache_dir' => './test/.cache'
                ],
            ],
        ]);
    }

    public function addItem($key, $value) {
        return $this->storage->addItem($key, json_encode($value));
    }

    public function getItem($key)
    {
        return json_decode($this->storage->getItem($key));
    }
}