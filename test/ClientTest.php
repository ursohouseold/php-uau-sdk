<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUauTest\Bootstrap;

class ClientTest extends TestCase
{
    private $client;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->client = $bootstrap->getClient();
    }

    public function testDeveFazerARequisicaoPostERetornar200()
    {
        $request = $this->client->post('/Autenticador/AutenticarUsuario', []);
        $this->assertTrue(is_int($request->getStatusCode()));
    }


    public function testDeveSetarOHeader() {
        $request = $this->client->get('/Autenticador/AutenticarUsuario');
        $this->assertTrue(is_int($request->getStatusCode()));
    }
}
