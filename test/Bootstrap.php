<?php
/**
 * @author Luã Mota <hi@mocallu.com>
 */
namespace PHPUauTest;

use PHPUau\Client;
use PHPUauTest\Cache;
use PHPUau\Services\Autenticador;

class Bootstrap
{
    private $client;
    private $config;
    private $autenticador;
    private $cache;

    public function __construct()
    {
        $this->client = new Client();
        $this->cache = new Cache();

        $this->config = $this->getConfig();
        // $this->client->setHeader('content-length', 1025);
        $this->client->setHeader('X-INTEGRATION-Authorization', $this->config['token']);
        $this->client->setBaseUri($this->config['base_uri']);
        $this->autenticador = new Autenticador($this->client);

        $token = $this->cache->getItem('token');
        if (!$token) {
            $response = $this->autenticador->login($this->config['login'], $this->config['senha']);
            $token = $response['data']['token'];
            $this->cache->addItem('token', $token);
        }
        $this->client->setHeader('Authorization', $token);
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getPessoaLogada()
    {
        $detalhesPessoa = $this->cache->getItem('detalhesPessoa');
        if(!$detalhesPessoa) {
            $detalhesPessoa = $this->autenticador->detalhesPessoa()['data'];
            $this->cache->addItem('detalhesPessoa', $detalhesPessoa);
        }
        return $detalhesPessoa;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config.php';
    }
}
