<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUau\Services\Boleto;
use PHPUauTest\Bootstrap;

class BoletoTest extends TestCase
{
    private $boleto;
    private $pessoa;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->boleto = new Boleto($bootstrap->getClient());
        $this->pessoa = $bootstrap->getPessoaLogada();
    }

    public function testDeveRetornarAListaDeBoletos()
    {
        $codigo = $this->pessoa->dadosPessoais->codigo;
        $response = $this->boleto->consultaBoletosByCodigoPessoa($codigo);
        $this->assertEquals(200, $response['request']->getStatusCode());
        $this->assertTrue(is_array($response['data']));
    }
}
