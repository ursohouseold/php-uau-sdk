<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUau\Services\Pessoa;
use PHPUauTest\Bootstrap;

class PessoaTest extends TestCase
{
    private $pessoa;
    private $config;
    private $pessoaLogada;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->pessoa = new Pessoa($bootstrap->getClient());
        $this->config = $bootstrap->getConfig();
        $this->pessoaLogada = $bootstrap->getPessoaLogada();
    }

    public function testDeveAlterarASenhaParaAMesmaDoArquivoDeConfig()
    {
        $codigo = $this->pessoaLogada->dadosPessoais->codigo;
        $response = $this->pessoa->alteraSenha($codigo, $this->config['senha']);
        $this->assertEquals(200, $response['request']->getStatusCode());
        $this->assertEquals('Senha alterada com sucesso!', $response['data']);
    }
}
