<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUau\Services\Autenticador;
use PHPUauTest\Bootstrap;

class AutenticadorTest extends TestCase
{
    private $autenticador;
    private $config;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->config = $bootstrap->getConfig();
        $this->autenticador = new Autenticador($bootstrap->getClient());
    }

    public function testDeveConseguirFazerOLoginERetornarOTokenDaPessoa()
    {
        $response = $this->autenticador->login($this->config['login'], $this->config['senha']);
        $this->assertEquals(200, $response['request']->getStatusCode());
    }

    public function testDeveRetornarOsDetalhesDaPessoa()
    {
        $response = $this->autenticador->detalhesPessoa();
        $this->assertEquals(200, $response['request']->getStatusCode());
    }
}
