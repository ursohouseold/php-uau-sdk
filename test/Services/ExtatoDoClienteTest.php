<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUau\Services\Venda;
use PHPUau\Services\ExtratoDoCliente;
use PHPUauTest\Bootstrap;

class ExtratoDoClienteTest extends TestCase
{
    private $extratoDoCliente;
    private $pessoaLogada;
    private $venda;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->extratoDoCliente = new ExtratoDoCliente($bootstrap->getClient());
        $this->venda = new Venda($bootstrap->getClient());
        $this->pessoaLogada = $bootstrap->getPessoaLogada();
    }

    public function testDeveRetornarOPdfComOExtratoDoCliente()
    {
        $codigo = $this->pessoaLogada->dadosPessoais->codigo;
        $response = $this->venda->listaEmpreendimentosPorPessoa($codigo);
        $emp = $response['data'][0];

        $response = $this->extratoDoCliente->geraPdfExtrato($codigo, $emp->Obra_Ven, $emp->Empresa_ven, $emp->Num_Ven);
        $this->assertEquals(200, $response['request']->getStatusCode());
        $this->assertTrue(is_string($response['data']));
    }
}
