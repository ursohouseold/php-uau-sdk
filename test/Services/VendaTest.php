<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUau\Services\Venda;
use PHPUauTest\Bootstrap;

class VendaTest extends TestCase
{
    private $venda;
    private $pessoaLogada;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->venda = new Venda($bootstrap->getClient());
        $this->pessoaLogada = $bootstrap->getPessoaLogada();
    }

    public function testDeveRetornarAListaDeEmpreendimentos()
    {
        $codigo = $this->pessoaLogada->dadosPessoais->codigo;
        $response = $this->venda->listaEmpreendimentosPorPessoa($codigo);
        $this->assertEquals(200, $response['request']->getStatusCode());
        $this->assertTrue(is_array($response['data']));
    }
}
