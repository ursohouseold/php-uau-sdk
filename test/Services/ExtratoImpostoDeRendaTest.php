<?php
namespace PHPUauTest;

use PHPUnit\Framework\TestCase;
use PHPUau\Services\Venda;
use PHPUau\Services\ExtratoImpostoDeRenda;
use PHPUauTest\Bootstrap;

class ExtratoImpostoDeRendaTest extends TestCase
{
    private $pessoaLogada;

    public function setUp()
    {
        $bootstrap = new Bootstrap();
        $this->pessoaLogada = $bootstrap->getPessoaLogada();
        $this->vendas = new Venda($bootstrap->getClient());
        $this->impostoDeRenda = new ExtratoImpostoDeRenda($bootstrap->getClient());
    }

    public function testDeveRetornarPdfDoImpostoDeRenda()
    {
        $codigo = $this->pessoaLogada->dadosPessoais->codigo;
        $response = $this->vendas->listaEmpreendimentosPorPessoa($codigo);
        $empresa = $response['data'][0];
        $p = [
            'ano'     => 2019,
            'venda'   => $empresa->Num_Ven,
            'obra'    => $empresa->Obra_Ven,
            'empresa' => $empresa->Empresa_ven
        ];
        $response = $this->impostoDeRenda->gerarPdfImpostoDeRenda($p['ano'], $p['venda'], $p['obra'], $p['empresa']);
        $this->assertEquals(200, $response['request']->getStatusCode(), $response['request']->getBody());
    }
}
